# wxfacepay-Iot-miniprogram
支付宝蜻蜓刷脸支付小程序  
配套微信Iot小程序：[微信青蛙刷脸支付小程序](https://gitee.com/maidoududuzai/wxfacepay-Iot-miniprogram)  
配套Web后端系统：[服务商刷脸支付系统](https://gitee.com/maidoududuzai/pay-crm-osc)  

# 测试账号
系统域名：demo.tryyun.net  
店员账号：12345678901 或 00000000000

# 联系方式
QQ：746954832  
官方QQ群：20533363  
![无锡创云网络科技](https://images.gitee.com/uploads/images/2020/0611/094157_1a5d7bd1_4857616.jpeg "qrcode_for_gh_693cd96fb541_258.jpg")